#pragma once

#include "math.h++"

template <int n>
class NoiseOpenSimplex
{
public:
	struct Data
	{
		integer li[n]; // Lattice integer coordinantes
		real lf[n];    // Lattice coordinates relative to corner
		real ef[n];     // Euclidean coordinates relative to corner
		real sum_lf;   // Sum of lf's
		integer simplex; // floor(offset)

		integer axis_major; // Largest axis
		integer edge; // Edge number

		real f; // Function evaluation

		Data(): simplex(-1) {}
	};
	explicit NoiseOpenSimplex(int seed = 0);
	~NoiseOpenSimplex();

	Data at(real const x[n]) const;

private:
	real* surflets_;
	int* perm_[n];
};

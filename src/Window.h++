#pragma once

#include <chrono>
#include <queue>
#include <unordered_map>

#include <GL/glew.h>

/**
 * Triggers an assertion if detects GL error.
 */
bool
assertGLNoError();

/**
 * @brief Register a GL shader program.
 * @param[out] program Non-null. Filled with program if successful.
 * @param[in] sourceVert Source of the vertex shader
 * @param[in] sourceFrag Source of the fragment shader
 * @return True if successful.
 */
GLuint
registerGLProgram(char const sourceVert[],
                  char const sourceFrag[]);

class GLFWwindow;


/**
 * Each enum entry corresponds to a key on the keyboard. The numbers are mapped
 * to GLFW keyboard macros.
 */
enum class Key
{
	Unknown = -1, // GLFW_KEY_UNKNOWN
	Space = 32, // GLFW_KEY_SPACE
	Apostrophe = 39, // GLFW_KEY_APOSTROPHE, /* ' */
	Comma = 44, // GLFW_KEY_COMMA, /* , */
	Minus, /* - */
	Period, /* . */
	Slash, /* / */
	N_0 = 48, // GLFW_KEY_0, // Numbers, 0
	N_1,
	N_2,
	N_3,
	N_4,
	N_5,
	N_6,
	N_7,
	N_8,
	N_9,
	Semicolon = 59, // GLFW_KEY_SEMICOLON, /* ; */
	Equal = 61, // GLFW_KEY_EQUAL, /* = */
	A = 65, // GLFW_KEY_A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	BracketLeft = 91, // GLFW_KEY_LEFT_BRACKET, /* [ */
	Backslash, /* \ */
	BracketRight, /* ] */
	Grave = 96, // GLFW_KEY_GRAVE_ACCENT, /* ` */
	World1 = 161, // GLFW_KEY_WORLD_1,
	World2,
	Escape = 256, // GLFW_KEY_ESCAPE,
	Enter,
	Tab,
	Backspace,
	Insert,
	Delete,
	Right,
	Left,
	Down,
	Up,
	PageUp,
	PageDown,
	Home,
	End,
	CapsLock = 280, // GLFW_KEY_CAPS_LOCK,
	ScrollLock,
	NumLock,
	PrintScreen,
	Pause,
	F1 = 290, // GLFW_KEY_F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
	F13,
	F14,
	F15,
	F16,
	F17,
	F18,
	F19,
	F20,
	F21,
	F22,
	F23,
	F24,
	F25,
	KP_0 = 320, // GLFW_KEY_KP_0, // Keypad
	KP_1,
	KP_2,
	KP_3,
	KP_4,
	KP_5,
	KP_6,
	KP_7,
	KP_8,
	KP_9,
	KP_Decimal,
	KP_Divide,
	KP_Multiple,
	KP_Subtract,
	KP_Add,
	KP_Enter,
	KP_Equal,
	ShiftLeft = 340, // GLFW_KEY_LEFT_SHIFT,
	ControlLeft,
	AltLeft,
	SuperLeft,
	ShiftRight,
	ControlRight,
	AltRight,
	SuperRight,
	Menu,
};
/**
 * Key modifiers
 */
enum class KeyMod
{
	Nothing = 0x0,
	Shift = 0x1,
	Control = 0x2,
	Alt = 0x4,
	Super = 0x8
};
/**
 * Key or mouse button actions
 */
enum class KeyAction
{
	Release = 0,
	Press,
	Repeat
};

/**
 * Wrapper around glfw
 */
class Window final
{
public:
	struct Action
	{
		Key key;
		KeyAction keya;
		KeyMod mod;
	};
	typedef std::queue<Action> ActionQueue;

	/**
	 * Must be called only once and before any window creation
	 */
	static void init();
	/**
	 * Creates a window with the given parameters.
	 *
	 * Warning: Created window must be activate()'d before use.
	 */
	static Window* create(int width, int height);


	~Window();
	/**
	 * Make this window the current window.
	 */
	void activate();

	/**
	 * Update the window status and return true if window should stay active.
	 */
	bool loop();

	int width() const { return width_; }
	int height() const { return height_; }

	KeyAction getKeyAction(Key) const;

	ActionQueue& actionQueue()
	{
		return actions_;
	}
	double frameDuration() const
	{
		return frameDuration_;
	}
	
private:
	static std::unordered_map<GLFWwindow*, Window*> windowMap;
	ActionQueue actions_;


	Window(GLFWwindow* const);
	
	GLFWwindow* const binding_;
	int width_, height_;

	std::chrono::time_point<std::chrono::high_resolution_clock> timeStamp_;
	double frameDuration_;
};



#pragma once

#include <iostream>

inline void assert(bool cond, char const msg[])
{
#ifndef NDEBUG
	if (!cond)
	{
		std::cerr << msg << "\n";
		throw false;
	}
#endif
}

inline void assertUnreachable(char const msg[] = "Unreachable")
{
	assert(false, msg);
}
inline void assertFail(char const msg[])
{
	assert(false, msg);
}


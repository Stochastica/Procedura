#include <cmath>
#include <cstring>
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Window.h++"
#include "renderer/RendererDefault.h++"
#include "renderer/RendererOpenSimplex.h++"
#include "util.h++"

int main(int argc, char* argv[])
{
	Window::init();

	Window* window = Window::create(1024, 768);
	window->activate();

	if (argc == 1)
	{
		std::cerr << "Please enter a mode: [default/osnoise]\n";
		return 1;
	}
	if (!std::strcmp(argv[1], "default"))
	{
		RendererDefault renderer(window);

		// Main loop
		renderer.start();
	}
	else if (!std::strcmp(argv[1], "osnoise"))
	{
		RendererOpenSimplex renderer(window);

		// Main loop
		renderer.start();
	}

	delete window;

	assertGLNoError();
	glfwTerminate();

	return 0;
}

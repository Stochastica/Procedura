#include "RendererDefault.h++"

#include <iostream>
#include <cmath>
#include "../Window.h++"

void
RendererDefault::init()
{
	int const width = window_->width();
	int const height = window_->height();

	texture_ = new float[width * height * 4];
	data_ = new float[width * height];

	reset();
}
RendererDefault::~RendererDefault()
{
	delete texture_;
}


void
RendererDefault::reset()
{
	int const width = window_->width();
	int const height = window_->height();

	x_ = y_ = 0.0;
	// Smallest dimension needs to contain 5 pixels.
	scale_ = 5.0 / std::min(width, height);
	dirty_ = true;
}

void*
RendererDefault::texture()
{
	double const movement = 1000.0 * window_->frameDuration() * scale_;
	double const zoom = std::exp(window_->frameDuration());
	if (window_->getKeyAction(Key::Left) == KeyAction::Press)
	{
		x_ -= movement;
		dirty_ = true;
	}
	if (window_->getKeyAction(Key::Right) == KeyAction::Press)
	{
		x_ += movement;
		dirty_ = true;
	}
	if (window_->getKeyAction(Key::Up) == KeyAction::Press)
	{
		y_ += movement;
		dirty_ = true;
	}
	if (window_->getKeyAction(Key::Down) == KeyAction::Press)
	{
		y_ -= movement;
		dirty_ = true;
	}
	if (window_->getKeyAction(Key::Equal) == KeyAction::Press)
	{
		scale_ *= zoom;
		dirty_ = true;
	}
	if (window_->getKeyAction(Key::Minus) == KeyAction::Press)
	{
		scale_ /= zoom;
		dirty_ = true;
	}

	if (!dirty_)
	{
		return nullptr;
	}

	
	int const width = window_->width();
	int const height = window_->height();
	if (dirty_)
	{
		// Recompute the entire image
		int const middle_i = width / 2;
		int const middle_j = height / 2;
		for (int j = 0; j < height; ++j)
			for (int i = 0; i < width; ++i)
			{
				int const index = j * width + i;

				double const x[] = {
					(i - middle_i) * scale_ + x_,
					(j - middle_j) * scale_ + y_,
				};
				data_[index] = std::exp(- x[0] * x[0] - x[1] * x[1]);
			}

		for (int j = 0; j < height; ++j)
			for (int i = 0; i < width; ++i)
			{
				int const index = j * width + i;

				float const val = data_[index];

				texture_[index * 4 + 0] = val;
				texture_[index * 4 + 1] = val;
				texture_[index * 4 + 2] = val;
				texture_[index * 4 + 3] = 1.0;
			}
	}

	std::cout << "Update: [" << x_ << ", " << y_ << ": " << scale_ << "]"
	          << "\n" << "\e[1A";

	dirty_ = false;

	return texture_;
}

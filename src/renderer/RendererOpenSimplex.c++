#include "RendererOpenSimplex.h++"

#include <cmath>

#include "util.h++"
#include "../util.h++"
#include "../Window.h++"

void
RendererOpenSimplex::init()
{
	initRenderUtils();

	int const width = window_->width();
	int const height = window_->height();

	data2 = new NoiseOpenSimplex<2>::Data[width * height];
	texture_ = new float[width * height * 4];

	reset();
}

RendererOpenSimplex::~RendererOpenSimplex()
{
	delete texture_;
}

void
RendererOpenSimplex::reset()
{
	int const width = window_->width();
	int const height = window_->height();
	mode_ = Mode::Output;

	x_ = y_ = 0.0;
	// Smallest dimension needs to contain 5 pixels.
	scale_ = 5.0 / std::min(width, height);
	dirtyMode_ = true;
	dirty_ = true;

}
void*
RendererOpenSimplex::texture()
{
	// Pull all actions from action queue
	//auto aqueue = window_->actionQueue();

	// Handle navigation
	double const movement = 1000.0 * window_->frameDuration() * scale_;
	double const zoom = std::exp(window_->frameDuration());
	if (window_->getKeyAction(Key::Left) == KeyAction::Press)
	{
		x_ -= movement;
		dirtyMode_ = dirty_ = true;
	}
	if (window_->getKeyAction(Key::Right) == KeyAction::Press)
	{
		x_ += movement;
		dirtyMode_ = dirty_ = true;
	}
	if (window_->getKeyAction(Key::Up) == KeyAction::Press)
	{
		y_ += movement;
		dirtyMode_ = dirty_ = true;
	}
	if (window_->getKeyAction(Key::Down) == KeyAction::Press)
	{
		y_ -= movement;
		dirtyMode_ = dirty_ = true;
	}
	if (window_->getKeyAction(Key::Equal) == KeyAction::Press)
	{
		scale_ *= zoom;
		dirtyMode_ = dirty_ = true;
	}
	if (window_->getKeyAction(Key::Minus) == KeyAction::Press)
	{
		scale_ /= zoom;
		dirtyMode_ = dirty_ = true;
	}

#define MODE_CASE(key, mode) \
	if (window_->getKeyAction(Key::key) == KeyAction::Press && mode_ != Mode::mode) \
	{ \
		mode_ = Mode::mode; \
		dirtyMode_ = true; \
	}
	MODE_CASE(Q, Simplex);
	MODE_CASE(W, CentralAxis);
	MODE_CASE(A, Output);
	MODE_CASE(S, OutputWithLattice);
	if (window_->getKeyAction(Key::Z) == KeyAction::Press)
	{
		reset();
		dirtyMode_ = dirty_ = true;
	}


	if (!dirty_ && !dirtyMode_)
	{
		return nullptr;
	}

	
	int const width = window_->width();
	int const height = window_->height();
	if (dirty_)
	{
		min_ = std::numeric_limits<real>::infinity();
		max_ = -std::numeric_limits<real>::infinity();
		// Recompute the entire image
		int const middle_i = width / 2;
		int const middle_j = height / 2;
		for (int j = 0; j < height; ++j)
			for (int i = 0; i < width; ++i)
			{
				int const index = j * width + i;

				double const x[] = {
					(i - middle_i) * scale_ + x_,
					(j - middle_j) * scale_ + y_,
				};
				data2[index] = noise2.at(x);

				min_ = std::min(data2[index].f, min_);
				max_ = std::max(data2[index].f, max_);
			}
	}
	if (dirtyMode_)
	{
		switch (mode_)
		{
		case Mode::Simplex:
			for (int j = 0; j < height; ++j)
				for (int i = 0; i < width; ++i)
				{
					int const index = j * width + i;

					getRandomColor(&texture_[index * 4], data2[index].simplex);
					texture_[index * 4 + 3] = 1.0;
				}
			break;
		case Mode::CentralAxis:
			for (int j = 0; j < height; ++j)
				for (int i = 0; i < width; ++i)
				{
					int const index = j * width + i;

					real const val = data2[index].sum_lf / 2;

					texture_[index * 4 + 0] = val;
					texture_[index * 4 + 1] = val;
					texture_[index * 4 + 2] = val;
					texture_[index * 4 + 3] = 1.0;
				}
			break;
		case Mode::Output:
		{
			real const range = 1.0 / (max_ - min_);
			for (int j = 0; j < height; ++j)
				for (int i = 0; i < width; ++i)
				{
					int const index = j * width + i;

					real const val = (data2[index].f - min_) * range;

					texture_[index * 4 + 0] = val;
					texture_[index * 4 + 1] = val;
					texture_[index * 4 + 2] = val;
					texture_[index * 4 + 3] = 1.0;
				}
			break;
		}
		case Mode::OutputWithLattice:
		{
			real const range = 1.0 / (max_ - min_);
			for (int j = 0; j < height; ++j)
				for (int i = 0; i < width; ++i)
				{
					int const index = j * width + i;

					getRandomColor(&texture_[index * 4], data2[index].simplex);
					real const val = (data2[index].f - min_) * range;

					texture_[index * 4 + 0] *= val;
					texture_[index * 4 + 1] *= val;
					texture_[index * 4 + 2] *= val;
					texture_[index * 4 + 3] = 1.0;
				}
			break;
		}
		default: assertUnreachable();
		}
	}

	std::cout << "Update: [" << x_ << ", " << y_ << ": " << scale_ << "]"
	          << " L:[" << min_ << ", " << max_ << "]"
	          << " M:" << (int) mode_
	          << "\n" << "\e[1A";


	dirty_ = false;
	dirtyMode_ = false;

	return texture_;
}

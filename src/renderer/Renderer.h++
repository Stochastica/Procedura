#pragma once

class Window;

/**
 * Stores internal data to this renderer.
 */
struct RendererData;

/**
 * Base class of all texture visualisation routines
 *
 * This class should not be constructed without a GL context present
 */
class Renderer
{
public:
	Renderer(Window* w);
	virtual ~Renderer();

	/**
	 * Main loop of the renderer
	 */
	void start();
protected:
	virtual void init() {}
	/**
	 * Returns an array of width * height * 4 of floats, each representing a
	 * single channel on a given pixel.
	 *
	 * Return null to indicate that the texture has not changed.
	 */
	virtual void* texture() = 0;

	Window* window_;
	RendererData* data_;
};

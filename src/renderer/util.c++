#include "util.h++"

#include <cmath>
#include <random>

#include "../util.h++"

namespace
{
	// Some random colors that correspond to the integers.
	constexpr int NRANDOM_COLORS = 12;
	float* randomColors;

	void generateRandomColors()
	{
		static bool initialised = false;
		if (initialised)
			return;
		initialised = true;

		std::default_random_engine generator(123);
		std::uniform_real_distribution<float> distribution(0.f, M_PI * 2);

		randomColors = new float[NRANDOM_COLORS * 3];
		for (int i = 0; i < NRANDOM_COLORS; ++i)
		{
			float const hue = distribution(generator);

			float const x = std::cos(hue);
			float const y = std::sin(hue);
			randomColors[i * 3 + 0] = (x + 1) / 2;
			randomColors[i * 3 + 1] = (x * -0.5 + y * 0.866025 + 1) / 2;
			randomColors[i * 3 + 2] = (x * -0.5 + y * -0.866025 + 1) / 2;
		}
		randomColors[0] = 0.0;
		randomColors[1] = 0.5;
		randomColors[2] = 1.0;

		randomColors[3] = 1.0;
		randomColors[4] = 0.5;
		randomColors[5] = 0.0;
	}

} // namespace <anonymous>

void
initRenderUtils()
{
	generateRandomColors();
}

void
getRandomColor(float out[3], int i)
{
	assert(-1 <= i && i < NRANDOM_COLORS, "Out of bound");

	if (i == -1)
	{
		out[0] = out[1] = out[2] = 0.0;
	}
	else
	{
		out[0] = randomColors[i * 3 + 0];
		out[1] = randomColors[i * 3 + 1];
		out[2] = randomColors[i * 3 + 2];
	}
}
